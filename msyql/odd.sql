-- 1、使用按位与
 -- 奇数
select * from table WHERE id & 1;
-- 偶数 【 id先除以2然后乘2 如果与原来的相等就是偶数】
select * from table WHERE id=(id>>1)<<1;

-- 2、正则匹配 正则匹配最后一位
-- 奇数
select * from table WHERE id regexp '[13579]$';
-- 偶数
select * from table WHERE id regexp '[02468]$';

-- 3、除2取余
-- 奇数
select * from table WHERE id % 2 = 1;
select * from table WHERE mod(id, 2) = 1;
-- 偶数
select * from table WHERE id % 2 = 0;
select * from table WHERE mod(id, 2) = 0;

-- 4、-1的奇数次方和偶数次方
-- 奇数
select * from table WHERE POWER(-1, id) = -1;
-- 偶数
select * from table WHERE POWER(-1, id) = 1;