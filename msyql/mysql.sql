REVERSE
(SUBSTR(REVERSE (g.tree_id), INSTR(REVERSE (g.tree_id), ',') + 1))
AS NAME REVERSE
(LEFT (REVERSE (g.tree_id), LOCATE(',', REVERSE (g.tree_id)) - 1))
AS NAME;

-- 查询重复数据
SELECT *
FROM `functions`
WHERE id NOT IN
      (SELECT MAX(id)
       FROM `functions`
       GROUP BY identify);

-- 删除重复数据
DELETE
FROM `functions`
WHERE id NOT IN (
    SELECT id
    FROM (
             SELECT id
             FROM `functions`
             WHERE id IN (SELECT id FROM `functions` GROUP BY identify HAVING COUNT(id) > 1)
         ) a
);

-- 复制数据
UPDATE profession_site
SET profession_type = 0;
INSERT INTO `profession_site`(profession_id, place_id, create_by, create_at, profession_type)
SELECT profession_id, place_id, create_by, NOW(), 1
FROM profession_site;

-- 自增序号
SELECT
         (@i := @i + 1)
     FROM
             (SELECT @i := 0) it;

-- 备份表
create table test_backup like test;
insert INTO test_backup select * from test;

-- 时间自增和修改
ALTER TABLE `chapter_data`
add COLUMN `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
add COLUMN `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
add COLUMN `create_by` varchar(50) NULL DEFAULT NULL COMMENT '创建人',
add COLUMN `update_by` varchar(50) NULL DEFAULT NULL COMMENT '修改人',
add COLUMN `del_flag` tinyint NULL DEFAULT 0 COMMENT '删除标识 0-未删除 1-已删除';

"pay_engine" --column-statistics=0 --result-file="/Users/zhanggj/Desktop/{data_source}-{timestamp}-dump.sql" --complete-insert

-- 截取两个字符之间对内容
SELECT SUBSTRING_INDEX(SUBSTRING_INDEX('zhanggengjia@ztoec.com', '@', -1), '.', 1);

-- group by 组内排序

# group by 组内排序
# 在SQL中，GROUP BY 语句用于按一个或多个列对结果集进行分组，并且可以配合 ORDER BY 语句来对分组后的结果进行排序。
#
# 如果您想要在每个分组内进行排序，可以使用 GROUP BY 语句配合 ORDER BY 语句，并且在 ORDER BY 中使用窗口函数（如 ROW_NUMBER()）来为每个分组内的行分配一个唯一的序号。
#
# 以下是一个简单的例子，假设我们有一个名为 sales 的表，它有 employee_id 和 sale_amount 两个字段，我们想要按 employee_id 分组，并且在每个组内按 sale_amount 降序排序：
SELECT employee_id, sale_amount
FROM (
    SELECT employee_id,
           sale_amount,
           ROW_NUMBER() OVER (PARTITION BY employee_id ORDER BY sale_amount DESC) AS rn
    FROM sales
) AS grouped_sales
WHERE rn = 1;
# 在这个例子中，我们首先定义了一个子查询，它使用 ROW_NUMBER() 窗口函数按 employee_id 分组，并按 sale_amount 降序排序每个组。然后，外层查询选择每个组中 rn 值为 1 的记录，即每个组内 sale_amount 最高的记录。


-- 创建查询用户
SELECT User, Host FROM mysql.user;

SHOW GRANTS;

CREATE USER 'query_user'@'%' IDENTIFIED BY 'TzRuj8OaxxzZqRyxr';
GRANT SELECT ON homework_prod.* TO 'query_user'@'%';
FLUSH PRIVILEGES;
-- 创建查询用户

-- kill锁表
# trx_mysql_thread_id
SELECT * FROM information_schema.INNODB_TRX WHERE trx_state = 'LOCK WAIT';
# kill 408194;