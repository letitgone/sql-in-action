date ： “yyyy-mm-dd”格式表示的日期值
time ： “hh:mm:ss”格式表示的时间值
datetime： “yyyy-mm-dd hh:mm:ss”格式
timestamp： “yyyymmddhhmmss”格式表示的时间戳值
year： “yyyy”格式的年份值。
范围：
date “1000-01-01” 到 “9999-12-31” 3字节
time “-838:59:59” 到 “838:59:59” 3字节
datetime “1000-01-01 00:00:00” 到 “9999-12-31 23:59:59” 8字节
timestamp 19700101000000 到 2037 年的某个时刻 4字节
year 1901 到 2155 1 字节