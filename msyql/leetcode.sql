select
    employee_id,
    if (
        mod(employee_id, 2) = 1 && left(name, 1) != 'M', salary, 0
    ) bonus
from Employees order by employee_id;

-- 删除重复数据，保留最小ID的一个
DELETE p1 FROM Person p1,
    Person p2
WHERE
    p1.Email = p2.Email AND p1.Id > p2.Id;

-- 首字母大写
SELECT user_id,
    CONCAT(UPPER(LEFT(name,1)),LOWER(SUBSTRING(name,2))) name
FROM Users
ORDER BY user_id

-- LIMIT
第一个参数指定第一个返回记录行的偏移量，第二个参数指定返回记录行的最大数目

-- DATEDIFF
SELECT
    weather.id AS 'Id'
FROM
    weather
        JOIN
    weather w ON DATEDIFF(weather.date, w.date) = 1
        AND weather.Temperature > w.Temperature;

-- 查询最多的一条
SELECT
    customer_number
FROM
    orders
GROUP BY customer_number
ORDER BY COUNT(*) DESC
LIMIT 1;