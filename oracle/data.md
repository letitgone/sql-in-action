Date 类型的内部编码为12
长度:占用7个字节
数据存储的每一位到第七位分别为：世纪，年，月，日，时，分，秒
TIMESTAMP是支持小数秒和时区的日期/时间类型。对秒的精确度更高
TIMESTAMP WITH TIME ZONE 类型是 TIMESTAMP 的子类型，增加了时区支持，占用13字节的存储空间，最后两位用于保存时区信息
INTERVAL 用于表示一段时间或一个时间间隔的方法。在前面有多次提过。INTERVAL有两种类型.
YEAR TO MONTH 能存储年或月指定的一个时间段.
DATE TO SECOND 存储天,小时,分钟,秒指定的时间段.